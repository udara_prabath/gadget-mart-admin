import axios from 'axios';

export const baseUrl = {
  // baseURL: 'http://localhost:8080',
  // baseURL: 'http://tasklab.ceyentra.lk/tasklab_api' /*Production server*/
  baseURL: 'http://128.199.30.5/gadget_mart'
};

const instance = axios.create({
  baseURL: baseUrl.baseURL
});


export default instance;
